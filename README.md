# EpicsScripts


**e3modulelist.sh** is a simple script to check the modules and their version which are available in your EPICS environment.

Once you source your EPICS env you can launch the script with these two options:


`> e3modulelist.sh --all`

it will then print all the available modules.


`> e3modulelist.sh -v <modulename>`

will give the version of your module

E.g.


`alfiorizzo@debian: $ e3modulelist.sh --all`  
`ADAndor   ADCSimDetector  ADPointGrey    ADSupport  busy      delaygen  i2cDev    ipmiComm    memDisplay  NDDriverStdArrays  regdev     sscan   symbolname   
ADAndor3  ADPluginCalib   ADProsilica    asyn       calc      devlib2   iocStats  keypress    modbus      pcre               s7plc      std     sysfs       
ADCore    ADPluginEdge    ADSimDetector  autosave   caPutLog  ess       ip        MCoreUtils  mrfioc2     recsync            sequencer  stream `


`alfiorizzo@debian:~ $ e3modulelist.sh -v recsync`

`1.3.0-9705e52`    