#!/bin/bash


#   author  : Alfio Rizzo
#   email   : alfio.rizzo@esss.se
#   date    : Fri July 5 17:02:07 CEST 2019
#   version : 1.0


function usage {
        echo "";
        echo "Usage    : $(basename "$0") [ OPTION ] ";
        echo "         [ OPTION ]";
        echo "              --all : print all modules available";
        echo "              -v <modulename> : print module version";
        echo "              --help : print this help and exit";    
        echo "";
    
}
if [ -z $E3_SITEMODS_PATH ]; then
    echo "Please setup your EPICS env before, exit!";
    exit
else if [ -n "$1" ]; then            
        case $1 in
        --all)
            ls $E3_SITEMODS_PATH
            ;;
        -v)
            if [ -n "$2" ]; then
                ls $E3_SITEMODS_PATH/$2 2> /dev/null || echo "The module $2 does not exist"
            else
                echo "Please specify a module name"
            fi
            ;;
        --help | *)
            usage
            ;;
        esac
    else
        usage
    fi
fi





